package com.changetype.currency.webclient.impl;

import com.changetype.currency.expose.exception.BusinessExepcion;
import com.changetype.currency.expose.response.changetype.ChangeTypeApiResponse;
import com.changetype.currency.webclient.CreationResourceWebClient;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class CreationResourceWebClientImpl  implements CreationResourceWebClient {

    @Autowired
    RestTemplate restTemplate;

    Gson gson = new Gson();

    @Override
    public String getChangeTypeWebClient(String moneyOne, String moneTwo) throws Exception {
        ChangeTypeApiResponse changeTypeApiResponse=null;
        log.info("variables : moneyOne:"+moneyOne+" moneTwo="+moneTwo);
        String response;

        try {

            RestTemplate restTemplate = new RestTemplate();
            String fooResourceUrl
                    = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/"
                            + moneyOne+"/"+moneTwo+".json";
            response = restTemplate.getForObject(fooResourceUrl, String.class);
            log.info("respuesta :" + response);

        } catch (Exception e) {
            throw  new BusinessExepcion(
                    "00601",
                    HttpStatus.NOT_FOUND,
                    "BCP:Error al consultar Api externo"
                            +"https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/"
                            + moneyOne.toLowerCase()+"/"+moneTwo.toLowerCase()+".json"
                    );
        }

        return response;
    }


}
