package com.changetype.currency.expose.advice;


import com.changetype.currency.expose.exception.BusinessExepcion;
import com.changetype.currency.expose.exception.NotFoundExeption;
import com.changetype.currency.expose.exception.RequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(annotations = RestController.class )
public class ControllerAdvice {

    @ExceptionHandler(value= RequestException.class)
    public ResponseEntity<ErrorResponse> requestExceptionHandler(RequestException ex){

        ErrorResponse error = ErrorResponse
                .builder()
                .code("P-777")
                .message(ex.getMessage())
                .localizedMessage(ex.getLocalizedMessage())
                .build();

        return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value= BusinessExepcion.class)
    public ResponseEntity<ErrorResponse> bussinessExceptionHandler(BusinessExepcion ex){

        ErrorResponse error = ErrorResponse
                .builder()
                .code(ex.getCode()) // codigo personalizado
                .message(ex.getMessage())
                .localizedMessage(ex.getLocalizedMessage())
                .build();

        return new ResponseEntity<>(error,ex.getStatus());
    }

    @ExceptionHandler(value= NotFoundExeption.class)
    public ResponseEntity<ErrorResponse> notFoundExceptionHandler(NotFoundExeption ex){

        ErrorResponse error = ErrorResponse
                .builder()
                .code(ex.getCode()) // codigo personalizado
                .message(ex.getMessage())
                //.localizedMessage(ex.getLocalizedMessage())
                .build();

        return new ResponseEntity<>(error,ex.getStatus());
    }



}
