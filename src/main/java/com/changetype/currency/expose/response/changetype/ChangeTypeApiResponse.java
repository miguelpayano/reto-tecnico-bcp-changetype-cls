package com.changetype.currency.expose.response.changetype;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ChangeTypeApiResponse {
    private Date date;
    private BigDecimal money;
}
