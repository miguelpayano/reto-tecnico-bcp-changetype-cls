package com.changetype.currency.expose.mapper;

import com.changetype.currency.expose.response.changetype.ChangeTypeResponse;
import com.changetype.currency.model.ChangeTypeEntity;
import org.springframework.stereotype.Component;

@Component
public class ChangeTypeResponseMapper {

    public final static ChangeTypeResponse buildChangeType(ChangeTypeEntity changeTypeEntity){
        ChangeTypeResponse changeTypeResponse = new ChangeTypeResponse();
        changeTypeResponse.setId(changeTypeEntity.getId());
        changeTypeResponse.setDescription(changeTypeEntity.getDescription());
        changeTypeResponse.setMoneyOne(changeTypeEntity.getMoneyOne());
        changeTypeResponse.setMoneyTwo(changeTypeEntity.getMoneyTwo());
        changeTypeResponse.setValue(changeTypeEntity.getValue());
        changeTypeResponse.setDate(changeTypeEntity.getDate());
        return changeTypeResponse;
    }

}
