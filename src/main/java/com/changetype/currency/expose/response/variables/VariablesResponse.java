package com.changetype.currency.expose.response.variables;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VariablesResponse {
    private Integer id_variable;
    private String name;
    private String value;
}
