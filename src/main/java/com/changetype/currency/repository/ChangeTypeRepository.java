package com.changetype.currency.repository;

import com.changetype.currency.model.ChangeTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChangeTypeRepository extends JpaRepository<ChangeTypeEntity,Integer> {

    @Query("SELECT e FROM ChangeTypeEntity e")
    List<ChangeTypeEntity> getAllChangeType();

    @Query(value="SELECT e from ChangeTypeEntity e "
            + " where  e.moneyOne=?1" +
            " and e.moneyTwo =?2")
    Optional<ChangeTypeEntity> getchangeTypeByMoneyOneAndMoneyTwo (
            @Param("originCurrency") String originCurrency,
            @Param("destinationCurrency") String destinationCurrency);

}
