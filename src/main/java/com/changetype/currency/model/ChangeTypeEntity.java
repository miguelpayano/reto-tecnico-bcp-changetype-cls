package com.changetype.currency.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "change_type")
public class ChangeTypeEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "description")
    private String description;

    @Column(name = "money_one")
    private String moneyOne;

    @Column(name = "money_two")
    private String moneyTwo;

    @Column(name = "value", precision = 6, scale = 2)
    private BigDecimal value;

    @Column(name = "register_date")
    private String date;

}