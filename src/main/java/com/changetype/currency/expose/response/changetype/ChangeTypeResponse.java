package com.changetype.currency.expose.response.changetype;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ChangeTypeResponse {
    private Integer id;
    private String description;
    private String moneyOne;
    private String moneyTwo;
    private BigDecimal value;
    private String date;

}
