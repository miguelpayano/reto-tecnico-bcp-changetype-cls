package com.changetype.currency.service;

import com.changetype.currency.expose.response.changetype.ChangeTypeResponse;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

public interface ChangeTypeService {
    Observable<ChangeTypeResponse> getAllTypeResponse() throws Exception;
    Single<ChangeTypeResponse> getByChangeTypeApi(String moneyOne , String moneTwo) throws Exception;

    String getMatriz(Integer userValue) throws Exception;


}
