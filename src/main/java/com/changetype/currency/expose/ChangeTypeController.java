package com.changetype.currency.expose;

import com.changetype.currency.expose.response.changetype.ChangeTypeResponse;
import com.changetype.currency.service.ChangeTypeService;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/change-type")
@CrossOrigin
@Slf4j
public class ChangeTypeController {
    @Autowired
    private ChangeTypeService changeTypeService;

    @GetMapping(value = "")
    public Observable<ChangeTypeResponse> getAllChangeType() throws Exception {
        log.info("inicio de llamada a servicio getAllChangeType");
        return changeTypeService.getAllTypeResponse();
    }

    @GetMapping(value = "/currency")
    public Single<ChangeTypeResponse> getByFisrtsCurrencyAndSecondCurrency(
            @RequestParam String moneyOne,
            @RequestParam String moneTwo
    ) throws Exception {
        log.info("inicio de llamada a servicio getAllChangeType");
        return changeTypeService.getByChangeTypeApi(moneyOne,moneTwo);
    }

    @GetMapping(value="/matriz")
    public String getMatrizByValue(
            @RequestParam Integer userValue)
            throws Exception {
        log.info("inicio de llamada a servicio que genera la matriz");

        return  changeTypeService.getMatriz(userValue);
    }


}
