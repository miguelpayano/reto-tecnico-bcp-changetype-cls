FROM adoptopenjdk/openjdk11:jre-11.0.6_10-alpine
COPY "./target/ms-chagetype-0.0.1-SNAPSHOT.jar" "app.jar"
EXPOSE 8092
ENTRYPOINT ["java","-jar","app.jar"]
